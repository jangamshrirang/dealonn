import 'package:flutter/material.dart';
// import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../model/modellist.dart';
import './rechargescreen.dart';
import './walletsreen.dart';
import 'createcoupon/createcoupon.dart';
import './nearbyshop/nearbyshop.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // List items = [
  //   'https://image.shutterstock.com/image-photo/white-transparent-leaf-on-mirror-600w-1029171697.jpg'
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Deal Onn'),
      //   actions: [
      //     IconButton(icon: Icon(Icons.search), onPressed: () {}),
      //     IconButton(
      //         icon: Icon(Icons.notification_important), onPressed: () {}),
      //   ],
      // ),
      // drawer: Drawer(),

      // drawerScrimColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 4,
              width: double.infinity,
              child: imageSlider(context),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (_) => WalletScreen()));
                  },
                  child: Container(
                    width: 160,
                    height: 60,
                    decoration: BoxDecoration(
                        color: Colors.lightBlue,
                        borderRadius: BorderRadius.all(Radius.circular(100.0))),
                    // color: Colors.lightBlue,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: Text(
                            'Recahrge Wallet',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                          child: Text(
                            '0.0',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (_) => WalletScreen()));
                  },
                  child: Container(
                    width: 160,
                    height: 60,
                    decoration: BoxDecoration(
                        color: Colors.lightBlue,
                        borderRadius: BorderRadius.all(Radius.circular(100.0))),
                    // color: Colors.lightBlue,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: Text(
                            'Recahrge Wallet',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                          child: Text(
                            '0.0',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (_) => CreateCoupon()));
                  },
                  child: Container(
                    width: 160,
                    height: 60,
                    decoration: BoxDecoration(
                        color: Colors.lightBlue,
                        borderRadius: BorderRadius.all(Radius.circular(100.0))),
                    // color: Colors.lightBlue,
                    child: Column(
                      children: [
                        Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Icon(
                              Icons.wallet_membership,
                              size: 20,
                              color: Colors.white,
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                          child: Text(
                            'Create Coupon',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => NearByShop()));
                  },
                  child: Container(
                    width: 160,
                    height: 60,
                    decoration: BoxDecoration(
                        color: Colors.lightBlue,
                        borderRadius: BorderRadius.all(Radius.circular(100.0))),
                    // color: Colors.lightBlue,
                    child: Column(
                      children: [
                        Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Icon(
                              Icons.near_me,
                              size: 20,
                              color: Colors.white,
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                          child: Text(
                            'Near By Shop',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Container(
                height: MediaQuery.of(context).size.height / 6,
                child: imageSlider(context)),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 155,
              child: GridView.builder(
                  // scrollDirection: Axis.horizontal,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 3 / 2,
                      crossAxisCount: 4,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10),
                  itemCount: deal.length,
                  itemBuilder: (context, index) {
                    return
                        // padding: const EdgeInsets.all(0),

                        InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => RechargeScreen()));
                      },
                      child: Container(
                        height: 180,
                        child: Column(
                          children: [
                            Container(
                              height: 40,
                              child: CircleAvatar(
                                child: ClipOval(child: deal[index].image),
                                radius: 50,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            //   CircleAvatar(

                            //   // backgroundImage: deal[index].image,
                            // ),
                            Container(height: 15, child: Text(deal[index].name))
                          ],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
        // )
        // ],
      ),
    );
  }
}

Swiper imageSlider(context) {
  return new Swiper(
    autoplay: true,
    itemBuilder: (BuildContext context, int index) {
      return new Image.network(
        "https://images.unsplash.com/photo-1595445364671-15205e6c380c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=764&q=80",
        fit: BoxFit.fill,
        // width: double.infinity,
        // height: 500,
      );
    },
    itemWidth: double.infinity,
    itemCount: 3,
    pagination: new SwiperPagination(),
    // viewportFraction: 0.8,
    scale: 0.9,
    // control: new SwiperControl(),
  );
}
