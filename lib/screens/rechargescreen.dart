import 'package:flutter/material.dart';

class RechargeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          // title: ,

          ),
      body: Form(
        child: Container(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Container(
                height: 60,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      border: new OutlineInputBorder(
                          //  borderRadius:  BorderRadius.all( Rad)
                          borderSide: new BorderSide(color: Colors.teal),
                          borderRadius:
                              BorderRadius.all(Radius.circular(100.0))),
                      // hintText: 'Tell us about yourself',
                      // helperText: 'Keep it short, this is just a demo.',
                      labelText: 'Select Operater',
                      prefixIcon: const Icon(
                        Icons.person,
                        color: Colors.lightBlueAccent,
                      ),
                      // prefixText: ' ',
                      // suffixText: 'USD',
                      // suffixStyle: const TextStyle(color: Colors.green)
                    ),
                  ),
                ),
              ),
              // )
              Container(
                height: 60,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                        border: new OutlineInputBorder(
                            //  borderRadius:  BorderRadius.all( Rad)
                            borderSide: new BorderSide(color: Colors.teal),
                            borderRadius:
                                BorderRadius.all(Radius.circular(100.0))),
                        // hintText: 'Tell us about yourself',
                        // helperText: 'Keep it short, this is just a demo.',
                        labelText: 'Mobile Number',
                        prefixIcon: Container(
                          height: 10,
                          child: Image.asset(
                            'assets/images/mobile.png',
                            height: 10,
                            width: 10,
                          ),
                        )
                        // const Icon(
                        //   Icons.person,
                        //   color: Colors.green,
                        // ),
                        // prefixText: ' ',
                        // suffixText: 'USD',
                        // suffixStyle: const TextStyle(color: Colors.green)
                        ),
                  ),
                ),
              ),
              Container(
                height: 60,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      border: new OutlineInputBorder(
                          //  borderRadius:  BorderRadius.all( Rad)
                          borderSide: new BorderSide(color: Colors.teal),
                          borderRadius:
                              BorderRadius.all(Radius.circular(100.0))),
                      // hintText: 'Tell us about yourself',
                      // helperText: 'Keep it short, this is just a demo.',
                      labelText: 'Amount',
                      prefixIcon: const Icon(
                        Icons.money,
                        color: Colors.lightBlueAccent,
                      ),
                      // prefixText: ' ',
                      // suffixText: 'USD',
                      // suffixStyle: const TextStyle(color: Colors.green)
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                  color: Colors.lightBlueAccent,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.lightBlueAccent)),
                  child: Text(
                    'PROCEED TO REACHARGE',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {})
            ],
          ),
        ),
      ),
    );
  }
}
