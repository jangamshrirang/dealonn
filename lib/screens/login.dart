import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Form(
        child: Container(
          child: Column(
            children: [
              TextFormField(),
              TextFormField(),
              RaisedButton(child: Text('Login'), onPressed: () {}),
            ],
          ),
        ),
      ),
    );
  }
}
