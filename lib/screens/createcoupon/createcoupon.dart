import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class CreateCoupon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 45.0,
          width: 120.0,
          // color: Colors.amber,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(100.0))),
          // color: Colors.transparent,
          child: Image.asset(
            'assets/images/logo.png',
            fit: BoxFit.fitWidth,
            height: 30,
            width: 30,
          ),
        ),
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: InputDecoration(
                  border: new OutlineInputBorder(borderSide: new BorderSide()),
                  prefixIcon: Icon(Icons.search),
                  hintText: 'Serach for coupon',
                  // helperText: 'Keep it short, this is just a demo.',
                  labelText: 'Search',
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  'ABC SHOP',
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            // SizedBox(
            //   height: 5,
            // ),
            Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Row(
                  children: [
                    Icon(Icons.star),
                    Icon(Icons.star),
                    Icon(Icons.star),
                    Icon(Icons.star),
                    Icon(Icons.star),
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Container(
                  height: 120,
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.network(
                      "https://images.unsplash.com/photo-1595445364671-15205e6c380c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=764&q=80",
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text('heading 1'),
                        SizedBox(
                          height: 5,
                        ),
                        Text('heading 1'),
                        SizedBox(
                          height: 5,
                        ),
                        Text('heading 1'),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: 18,
                ),
                Container(
                  child: Column(
                    children: [
                      Text(' Create Coupon'),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        height: 40,
                        width: 120,
                        child: TextFormField(
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide()),
                            // prefixIcon: Icon(Icons.search),
                            hintText: 'Create coupon',
                            hintStyle: TextStyle(fontSize: 12),
                            // helperText: 'Keep it short, this is just a demo.',
                            labelText: 'Search',
                          ),
                        ),
                      ),
                      RaisedButton(
                          color: Colors.lightBlueAccent,
                          child: Text('submit'),
                          onPressed: () {})
                    ],
                  ),
                )
              ],
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
                  softWrap: true,
                  textAlign: TextAlign.justify,
                ),
              ),
            ),
            Container(
              height: 200,
              child: imageSlider(context),
            )
          ],
        ),
      ),
    );
  }

  Swiper imageSlider(context) {
    return new Swiper(
      autoplay: true,
      itemBuilder: (BuildContext context, int index) {
        return new Image.network(
          "https://images.unsplash.com/photo-1595445364671-15205e6c380c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=764&q=80",
          fit: BoxFit.fill,
          // width: double.infinity,
          // height: 500,
        );
      },
      itemWidth: double.infinity,
      itemCount: 3,
      pagination: new SwiperPagination(),
      // viewportFraction: 0.8,
      scale: 0.9,
      // control: new SwiperControl(),
    );
  }
}

// class SearchBarDemoHome extends StatefulWidget {
//   @override
//   _SearchBarDemoHomeState createState() => new _SearchBarDemoHomeState();
// }

// class _SearchBarDemoHomeState extends State<SearchBarDemoHome> {
//   SearchBar searchBar;
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//   AppBar buildAppBar(BuildContext context) {
//     return new AppBar(
//         title: new Text('Search Bar Demo'),
//         actions: [searchBar.getSearchAction(context)]);
//   }

//   void onSubmitted(String value) {
//     setState(() => _scaffoldKey.currentState
//         .showSnackBar(new SnackBar(content: new Text('You wrote $value!'))));
//   }

//   _SearchBarDemoHomeState() {
//     searchBar = new SearchBar(
//         inBar: false,
//         buildDefaultAppBar: buildAppBar,
//         setState: setState,
//         onSubmitted: onSubmitted,
//         onCleared: () {
//           print("cleared");
//         },
//         onClosed: () {
//           print("closed");
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: searchBar.build(context),
//       key: _scaffoldKey,
//       body: new Center(
//           child: new Text("Don't look at me! Press the search button!")),
//     );
//   }
// }
