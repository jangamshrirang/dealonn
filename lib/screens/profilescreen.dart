import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 45.0,
          width: 120.0,
          // color: Colors.amber,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(100.0))),
          // color: Colors.transparent,
          child: Image.asset(
            'assets/images/logo.png',
            fit: BoxFit.fitWidth,
            height: 30,
            width: 30,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.mode_edit,
                color: Colors.white,
              ),
              onPressed: () {}),
          IconButton(
              icon: Icon(
                Icons.notification_important,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Container(
              width: double.infinity,
              height: 75,
              child: CircleAvatar(
                radius: 40,
                child: Icon(
                  Icons.person,
                  size: 30,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              child: Center(
                child: Text(
                  'change image',

                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.lightBlueAccent,
                      fontWeight: FontWeight.bold),
                  // textAlign: Alignment.center,
                ),
              ),
            ),
            Form(
              child: Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: 60,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: new InputDecoration(
                            border: new OutlineInputBorder(
                                //  borderRadius:  BorderRadius.all( Rad)
                                borderSide: new BorderSide(color: Colors.teal),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0))),
                            // hintText: 'Tell us about yourself',
                            // helperText: 'Keep it short, this is just a demo.',
                            labelText: 'Dealonn',
                            prefixIcon: const Icon(
                              Icons.person,
                              color: Colors.lightBlueAccent,
                            ),
                            // prefixText: ' ',
                            // suffixText: 'USD',
                            // suffixStyle: const TextStyle(color: Colors.green)
                          ),
                        ),
                      ),
                    ),
                    // )
                    Container(
                      height: 60,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: new InputDecoration(
                              border: new OutlineInputBorder(
                                  //  borderRadius:  BorderRadius.all( Rad)
                                  borderSide:
                                      new BorderSide(color: Colors.teal),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(100.0))),
                              // hintText: 'Tell us about yourself',
                              // helperText: 'Keep it short, this is just a demo.',
                              labelText: 'Mobile Number',
                              prefixIcon: Container(
                                height: 10,
                                child: Image.asset(
                                  'assets/images/mobile.png',
                                  height: 10,
                                  width: 10,
                                ),
                              )
                              // const Icon(
                              //   Icons.person,
                              //   color: Colors.green,
                              // ),
                              // prefixText: ' ',
                              // suffixText: 'USD',
                              // suffixStyle: const TextStyle(color: Colors.green)
                              ),
                        ),
                      ),
                    ),
                    Container(
                      height: 60,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: new InputDecoration(
                            border: new OutlineInputBorder(
                                //  borderRadius:  BorderRadius.all( Rad)
                                borderSide: new BorderSide(color: Colors.teal),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0))),
                            // hintText: 'Tell us about yourself',
                            // helperText: 'Keep it short, this is just a demo.',
                            labelText: 'Amount',
                            prefixIcon: const Icon(
                              Icons.money,
                              color: Colors.lightBlueAccent,
                            ),
                            // prefixText: ' ',
                            // suffixText: 'USD',
                            // suffixStyle: const TextStyle(color: Colors.green)
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 60,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: new InputDecoration(
                            border: new OutlineInputBorder(
                                //  borderRadius:  BorderRadius.all( Rad)
                                borderSide: new BorderSide(color: Colors.teal),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0))),
                            // hintText: 'Tell us about yourself',
                            // helperText: 'Keep it short, this is just a demo.',
                            labelText: 'Email',
                            prefixIcon: const Icon(
                              Icons.money,
                              color: Colors.lightBlueAccent,
                            ),
                            // prefixText: ' ',
                            // suffixText: 'USD',
                            // suffixStyle: const TextStyle(color: Colors.green)
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 60,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: new InputDecoration(
                            border: new OutlineInputBorder(
                                //  borderRadius:  BorderRadius.all( Rad)
                                borderSide: new BorderSide(color: Colors.teal),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0))),
                            // hintText: 'Tell us about yourself',
                            // helperText: 'Keep it short, this is just a demo.',
                            labelText: 'Address',
                            prefixIcon: const Icon(
                              Icons.money,
                              color: Colors.lightBlueAccent,
                            ),
                            // prefixText: ' ',
                            // suffixText: 'USD',
                            // suffixStyle: const TextStyle(color: Colors.green)
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 60,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: new InputDecoration(
                            border: new OutlineInputBorder(
                                //  borderRadius:  BorderRadius.all( Rad)
                                borderSide: new BorderSide(color: Colors.teal),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0))),
                            // hintText: 'Tell us about yourself',
                            // helperText: 'Keep it short, this is just a demo.',
                            labelText: 'City',
                            prefixIcon: const Icon(
                              Icons.money,
                              color: Colors.lightBlueAccent,
                            ),
                            // prefixText: ' ',
                            // suffixText: 'USD',
                            // suffixStyle: const TextStyle(color: Colors.green)
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RaisedButton(
                        color: Colors.lightBlueAccent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.lightBlueAccent)),
                        child: Text(
                          'Update Profile ',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {})
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
