import 'package:flutter/material.dart';
import '../model/modellist.dart';

class WalletScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 45.0,
          width: 120.0,
          // color: Colors.amber,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(100.0))),
          // color: Colors.transparent,
          child: Image.asset(
            'assets/images/logo.png',
            fit: BoxFit.fitWidth,
            height: 30,
            width: 30,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.mode_edit,
                color: Colors.white,
              ),
              onPressed: () {}),
          IconButton(
              icon: Icon(
                Icons.notification_important,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 160,
                  height: 60,
                  // decoration: BoxDecoration(
                  //     color: Colors.lightBlue,
                  //     borderRadius: BorderRadius.all(Radius.circular(100.0))),
                  color: Colors.lightBlue,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: Text(
                          'Recahrge Wallet',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                        child: Text(
                          '0.0',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
                // SizedBox(
                //   width: 10,
                // ),
                Container(
                  width: 160,
                  height: 60,
                  // decoration: BoxDecoration(
                  //     color: Colors.lightBlue,
                  //     borderRadius: BorderRadius.all(Radius.circular(100.0))),
                  color: Colors.lightBlue,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: Text(
                          'Voucher Wallet',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                        child: Text(
                          '0.0',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text('Add Money'),
                Text('Wallet Transfer'),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              width: double.infinity,
              child: Center(
                  child: Text(
                'Transcation Histroy',
                style: TextStyle(fontSize: 18),
              )),
            ),
            Container(
              height: 455,
              child: ListView.builder(
                  itemCount: wallet.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: Container(
                        child: Column(
                          children: [
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Icon(
                                    Icons.arrow_circle_up_rounded,
                                    size: 45,
                                    color: Colors.lightBlueAccent,
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  children: [
                                    Text('Paid to'),
                                    SizedBox(
                                      height: 1,
                                    ),
                                    Text(wallet[index].name)
                                  ],
                                ),
                                SizedBox(
                                  width: 100,
                                ),
                                Text(wallet[index].money.toString())
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  wallet[index].date.toString(),
                                  textAlign: TextAlign.left,
                                ),
                                Text(
                                  wallet[index].debited,
                                  textAlign: TextAlign.right,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
