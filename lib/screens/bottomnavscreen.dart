import 'package:dealonn/screens/cartscreen.dart';
import 'package:flutter/material.dart';
import 'homesreen.dart';
import './referandearn.dart';
import './walletsreen.dart';
import './historyscreen.dart';
import './privacyscreen.dart';
import './helpsreen.dart';
import './aboutussreen.dart';
import './profilescreen.dart';
import './reviewscreen.dart';

class BottomNavScreen extends StatefulWidget {
  @override
  _BottomNavScreenState createState() => _BottomNavScreenState();
}

class _BottomNavScreenState extends State<BottomNavScreen> {
  List<Widget> _pages = [
    // Container(child: Center(child: Text('hii'))),
    HomeScreen(),
    CartScreen(),
    ReferAndEarn(),
    ReviewScreen(),

    // ProfileScreen(),
    // NotificationScreen(),
    // LiveScreen(),
  ];

  int _selectedPageIndex = 0;

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 45.0,
          width: 120.0,
          // color: Colors.amber,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(100.0))),
          // color: Colors.transparent,
          child: Image.asset(
            'assets/images/logo.png',
            fit: BoxFit.fitWidth,
            height: 30,
            width: 30,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
          IconButton(
              icon: Icon(
                Icons.notification_important,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),
      drawer:
          // DrawerScreen(            ),
          Drawer(
              child: ListView(
        // height: 500,

        children: <Widget>[
          Container(
            height: 250,
            child: DrawerHeader(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: 35,
                    child: Icon(
                      Icons.person,
                      size: 30,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Divider(),
                  // FutureBuilder(
                  //   future: variable,
                  //   builder: (context, snapshot) {
                  //     if (snapshot.hasData) {
                  //       // userState = snapshot.data['state'];
                  //       return Container(
                  //         child: Column(
                  //           children: [
                  //             Text(snapshot.data['name'] ?? ''),
                  //             Text(snapshot.data['gender'] ?? ''),
                  //           ],
                  //         ),
                  //       );
                  //     } else {
                  //       return CircularProgressIndicator();
                  //     }
                  //   },
                  // )
                ],
              ),
            ),
          ),
          ListTile(
            leading: Container(
              child: Image.asset(
                'assets/images/username.png',
                height: 25,
                width: 25,
              ),
            ),
            // trailing: Icon(
            //   Icons.person_add_alt_1,
            //   color: Colors.blue,
            // ),
            title: Text('Profile'),
            onTap: () {
              // // _auth.signOutGoogle();
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => ProfileScreen()));
            },
          ),
          Divider(
            height: 2,
            thickness: 2,
          ),
          ListTile(
            // focusColor: Colors.blue,
            leading: Container(
              child: Image.asset(
                'assets/images/wallet.png',
                height: 25,
                width: 25,
              ),
            ),
            // trailing: Icon(
            //   Icons.info,
            //   color: Colors.blue,
            // ),
            title: Text('My Wallet'),
            onTap: () {
              // // _auth.signOutGoogle();
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => WalletScreen()));
            },
          ),
          Divider(
            height: 2,
            thickness: 2,
          ),
          ListTile(
            leading: Container(
              child: Image.asset(
                'assets/images/hisotry.png',
                height: 25,
                width: 25,
              ),
            ),
            // trailing: Icon(
            //   Icons.contact_phone,
            //   color: Colors.blue,
            // ),
            title: Text('My History'),
            onTap: () {
              // // // _auth.signOutGoogle();
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => HistoryScreen()));
            },
          ),
          Divider(
            height: 2,
            thickness: 2,
          ),
          ListTile(
            leading: Container(
              child: Image.asset(
                'assets/images/privacy_policy.png',
                height: 25,
                width: 25,
              ),
            ),
            // trailing: Icon(
            //   Icons.logout,
            //   color: Colors.blue,
            // ),
            title: Text('Privacy Policy '),
            onTap: () {
              // _auth.signOutGoogle();
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PrivacyScreen()));
            },
          ),
          Divider(
            height: 2,
            thickness: 2,
          ),
          Container(
            width: double.infinity,
            height: 50,
            color: Colors.lightBlueAccent,
            child: Center(
                child: Text(
              'Communication',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 17),
            )),
          ),
          ListTile(
            leading: Container(
              child: Image.asset(
                'assets/images/help.png',
                height: 25,
                width: 25,
              ),
            ),
            // trailing: Icon(
            //   Icons.logout,
            //   color: Colors.blue,
            // ),
            title: Text('Help'),
            onTap: () {
              // _auth.signOutGoogle();
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => HelpScreen()));
            },
          ),
          Divider(
            height: 2,
            thickness: 2,
          ),
          ListTile(
            leading: Container(
              child: Image.asset(
                'assets/images/help.png',
                height: 25,
                width: 25,
              ),
            ),
            // trailing: Icon(
            //   Icons.logout,
            //   color: Colors.blue,
            // ),
            title: Text('About us'),
            onTap: () {
              // _auth.signOutGoogle();
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => AboutusScreen()));
            },
          ),
          Divider(
            height: 2,
            thickness: 2,
          ),
          ListTile(
            leading: Container(
              child: Image.asset(
                'assets/images/logout.png',
                height: 25,
                width: 25,
              ),
            ),
            // trailing: Icon(
            //   Icons.logout,
            //   color: Colors.blue,
            // ),
            title: Text('Logout'),
            onTap: () {
              // _auth.signOutGoogle();
              // Navigator.of(context)
              //     .push(MaterialPageRoute(builder: (_) => LoginPage()));
            },
          ),
          Divider(
            height: 2,
            thickness: 2,
          ),
          // ListView(
          //   children: [

          //   ],
          // )
        ],
      )),
      body: _pages[_selectedPageIndex],
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        type: BottomNavigationBarType.fixed,
        // showSelectedLabels: true,
        onTap: _selectPage,
        currentIndex: _selectedPageIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.contact_page),
            title: Text('Refer& Earn'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Cart'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.video_library),
            title: Text('Review'),
          ),
        ],
        // fixedColor: Color.fromRGBO(0, 124, 255, 1.0),
        // .fromRGBO(63, 64, 144, 1),
      ),
    );
  }
}
