import 'package:flutter/material.dart';

class ShopSubCategory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sub Category'),
      ),
      body: Container(
        child: Column(
          children: [
            ListTile(
              title: Center(child: Text('Clothings and kids wears')),
            ),
            ListTile(
              title: Center(child: Text('Clothings and Ladies wears')),
            ),
            ListTile(
              title: Center(child: Text('Tallors suiting and Shriting')),
            ),
            ListTile(
              title: Center(child: Text('UnderGarment women and kids')),
            ),
            ListTile(
              title: Center(child: Text('Footwear shops man')),
            ),
            ListTile(
              title: Center(child: Text('Footwear shops man')),
            ),
            ListTile(
              title: Center(child: Text(' Others')),
            ),
          ],
        ),
      ),
    );
  }
}
