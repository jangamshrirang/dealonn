import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:smart_select/smart_select.dart';
import './shopsubcategory.dart';

class NearByShop extends StatefulWidget {
  @override
  _NearByShopState createState() => _NearByShopState();
}

class _NearByShopState extends State<NearByShop> {
  String value;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 45.0,
          width: 120.0,
          // color: Colors.amber,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(100.0))),
          // color: Colors.transparent,
          child: Image.asset(
            'assets/images/logo.png',
            fit: BoxFit.fitWidth,
            height: 30,
            width: 30,
          ),
        ),
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          children: [
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text('Garments and Footwears')),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text('Cars and Bikes')),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text('Pizza, Cakes, Coffee')),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text('Jewllary and Watch')),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text('Reastrant  and Bar')),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text(' Medicals')),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text('Hardware')),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => ShopSubCategory()));
              },
              title: Center(child: Text(' Others')),
            ),
          ],
        ),
      ),
    );
  }
}
