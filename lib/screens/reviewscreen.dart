import 'package:flutter/material.dart';

class ReviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Container(
      //     height: 45.0,
      //     width: 120.0,
      //     // color: Colors.amber,
      //     decoration: BoxDecoration(
      //         color: Colors.white,
      //         borderRadius: BorderRadius.all(Radius.circular(100.0))),
      //     // color: Colors.transparent,
      //     child: Image.asset(
      //       'assets/images/logo.png',
      //       fit: BoxFit.fitWidth,
      //       height: 30,
      //       width: 30,
      //     ),
      //   ),
      //   actions: [
      //     IconButton(
      //         icon: Icon(
      //           Icons.mode_edit,
      //           color: Colors.white,
      //         ),
      //         onPressed: () {}),
      //     IconButton(
      //         icon: Icon(
      //           Icons.notification_important,
      //           color: Colors.white,
      //         ),
      //         onPressed: () {}),
      //   ],
      // ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 100,
                      color: Colors.lightBlueAccent,
                      width: 150,
                      child: Center(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Total Happy Vendors and Customer'),
                      )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: 150,
                      height: 100,
                      color: Colors.lightBlueAccent,
                      child: Center(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Total cash back earned by our customers'),
                      )),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.lightBlueAccent,
                height: 30,
                width: 300,
                child: Center(child: Text('Dealonn Rate and Review ')),
              ),
              ListTile(
                leading: Text('10/11/2021'),
                title: Text('Sudhir Jadhav'),
                subtitle: Row(
                  children: [
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                  ],
                ),
                trailing: Container(height: 20, child: Text('great service')),
                // Icon(Icons.star_outline),
                // Icon(Icons.star_outline),
                // Icon(Icons.star_outline),
              ),
              ListTile(
                leading: Text('10/11/2021'),
                title: Text('Rajesh kulkarni'),
                subtitle: Row(
                  children: [
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                  ],
                ),
                trailing: Container(height: 20, child: Text('great service')),
                // Icon(Icons.star_outline),
                // Icon(Icons.star_outline),
                // Icon(Icons.star_outline),
              ),
              ListTile(
                leading: Text('10/11/2021'),
                title: Text('Sunil sharma'),
                subtitle: Row(
                  children: [
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                    Icon(Icons.star_outline),
                    // Icon(Icons.star_outline),
                  ],
                ),
                trailing: Container(height: 20, child: Text('nice idea')),
                // Icon(Icons.star_outline),
                // Icon(Icons.star_outline),
                // Icon(Icons.star_outline),
              ),
              Container(
                width: 300,
                child: TextFormField(
                  decoration: new InputDecoration(
                    hintText: 'Comment',
                    border: new OutlineInputBorder(

                        //  borderRadius:  BorderRadius.all( Rad)
                        borderSide: new BorderSide(color: Colors.teal),
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  ),
                  maxLines: 4,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.star_outline),
                  Icon(Icons.star_outline),
                  Icon(Icons.star_outline),
                  Icon(Icons.star_outline),
                  Icon(Icons.star_outline),
                ],
              ),
              RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.lightBlueAccent)),
                  color: Colors.lightBlueAccent,
                  child: Text('Submit'),
                  onPressed: () {})
            ],
          ),
        ),
      ),
    );
  }
}
