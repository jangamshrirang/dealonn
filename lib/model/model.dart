import 'package:flutter/material.dart';

class DealModel {
  final String name;
  final Image image;

  DealModel({this.name, this.image});
}

class Wallet {
  final String name;
  final int money;
  final int date;
  final String debited;

  Wallet({this.name, this.money, this.date, this.debited});
}
