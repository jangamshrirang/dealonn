import 'package:flutter/cupertino.dart';

import './model.dart';

List<DealModel> deal = [
  DealModel(
      name: 'Mobile',
      image: Image.asset(
        'assets/images/mobile.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
  DealModel(
      name: 'DTH',
      image: Image.asset(
        'assets/images/dth.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
  DealModel(
      name: 'Electricity',
      image: Image.asset(
        'assets/images/billpaymentnew.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
  DealModel(
      name: 'Broadband',
      image: Image.asset(
        'assets/images/broadband.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
  DealModel(
      name: 'Gas',
      image: Image.asset(
        'assets/images/gas.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
  DealModel(
      name: 'FASTag',
      image: Image.asset(
        'assets/images/gas.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
  DealModel(
      name: 'Landline',
      image: Image.asset(
        'assets/images/landline.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
  DealModel(
      name: 'More',
      image: Image.asset(
        'assets/images/gas.png',
        fit: BoxFit.cover,
        height: 30,
        width: 30,
      )),
];
List<Wallet> wallet = [
  Wallet(name: 'sadan', money: 200, debited: 'debited from wallet', date: 11),
  Wallet(name: 'sadan', money: 200, debited: 'debited from wallet', date: 11),
  Wallet(name: 'sadan', money: 200, debited: 'debited from wallet', date: 11),
  Wallet(name: 'sadan', money: 200, debited: 'debited from wallet', date: 11),
  Wallet(name: 'sadan', money: 200, debited: 'debited from wallet', date: 11),
];
